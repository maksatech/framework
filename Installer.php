<?php
namespace Maksatech\Framework;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use ReflectionException;

/**
 * Class Installer
 * @package Maksatech\Framework
 */
class Installer extends Application
{
    /**
     * Installer constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param PackageEvent|null $event
     * @throws ReflectionException
     */
    public static function init(PackageEvent $event = null): void
    {
        \Maksatech\Core\Installer::init($event);
        Web\Installer::init($event);

        $config = self::getConfig();

        if($config->hasFile())
            $config->load();


        $method = new \ReflectionMethod(get_class($config),'put');

        if($method->isProtected() || $method->isPrivate())
            $method->setAccessible(true);

        $method->invokeArgs($config,[[
            'localizations_dir' => 'localizations',
            'timezone' => 'UTC',
            'database' => [
                'driver' => 'mysql',
                'host' => 'localhost',
                'port' => '3306',
                'database' => '',
                'username' => '',
                'password' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => ''
            ]
        ]]);


        if($config->has('localizations_dir') && !file_exists(self::systemRootDirPath().'/'.$config->get('localizations_dir'))) {
            mkdir(self::systemRootDirPath().'/'.$config->get('localizations_dir'));
        }

        if(file_exists(__DIR__.'/localizations')) {
            $dirs = glob(__DIR__.'/localizations/*');
            foreach ($dirs as $dir) {
                if(is_dir($dir)) {
                    $dirExplode = explode('/',$dir);
                    if(!file_exists(self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$dirExplode[count($dirExplode)-1])) {
                        mkdir(self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$dirExplode[count($dirExplode)-1]);
                    }

                    $dictionaries = glob($dir.'/*.php');
                    foreach ($dictionaries as $dictionary) {
                        $dictionaryExplode = explode('/',$dictionary);
                        if(!file_exists(self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$dirExplode[count($dirExplode)-1].'/'.$dictionaryExplode[count($dictionaryExplode)-1])) {
                            file_put_contents(self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$dirExplode[count($dirExplode)-1].'/'.$dictionaryExplode[count($dictionaryExplode)-1], file_get_contents($dictionary));
                        }
                    }
                }
            }
        }
    }
}
