<?php
namespace Maksatech\Framework\Web;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Maksatech\Http\Requests\RouterInstaller;
use Maksatech\Views\Rendering\ThemeInstaller;
use ReflectionException;

/**
 * Class Installer
 * @package Maksatech\Framework\Web
 */
class Installer extends Application
{

    /**
     * Installer constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param PackageEvent|null $event
     * @throws ReflectionException
     */
    public static function init(PackageEvent $event = null): void
    {
        RouterInstaller::init($event);
        ThemeInstaller::init($event);

        $config = self::getConfig();

        if($config->hasFile())
            $config->load();

        $method = new \ReflectionMethod(get_class($config),'put');

        if($method->isProtected() || $method->isPrivate())
            $method->setAccessible(true);

        $method->invokeArgs($config,[[
            'before_run' => [
                'Maksatech\\Framework\\Handlers::initDatabaseConfig'
            ],
            'after_run' => [

            ]
        ]]);

    }
}
