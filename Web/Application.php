<?php
namespace Maksatech\Framework\Web;

use Maksatech\Http\Requests\Router;
use Exception;

/**
 * Class Application
 * @package Maksatech\Framework\Web
 */
class Application extends \Maksatech\Framework\Application
{
    /**
     * @var string
     */
    protected static string $configNamespace = 'core';

    /**
     * @var string
     */
    protected static string $configDir = 'framework/web';

    /**
     * @var string
     */
    protected static string $configName = 'application';

    /**
     * Application constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @return void
     */
    public function run()
    {
        try {
            $this->beforeRun();
            $router = new Router();
            $router->setLocaleContainer($this);
            $router->setDatabaseContainer($this);
            $router->setInstancesContainer($this);
            $router->setRequestContainer($this);
            $router->setSiteContainer($this);
            $router->setViewsContainer($this);
            $router->request($_SERVER['REQUEST_URI'] ?? '/');
            $this->afterRun();
        }
        catch(\Exception $e) {
            die($e->getMessage());
        }
    }
}