<?php
namespace Maksatech\Framework;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request as IlluminateHttpRequest;
use Illuminate\Translation\Translator;
use Illuminate\Translation\ArrayLoader;
use Maksatech\Containers\Exceptions\LanguageNullException;
use Maksatech\Containers\LanguageInterface;
use Maksatech\Containers\RequestContainerInterface;
use Maksatech\Containers\SiteContainerInterface;
use Maksatech\Containers\SiteInterface;
use Maksatech\Containers\ViewsContainerInterface;
use Maksatech\Core\Core;
use Maksatech\Core\SetGetTrait;
use Maksatech\Containers\DatabaseContainerInterface;
use Maksatech\Containers\InstancesContainerInterface;
use Maksatech\Containers\LocaleContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Exception;
use ReflectionMethod;

/**
 * Class Application
 * @package Maksatech\Framework
 */
class Application extends Core implements LocaleContainerInterface, DatabaseContainerInterface, InstancesContainerInterface, RequestContainerInterface, SiteContainerInterface, ViewsContainerInterface
{
    use SetGetTrait;

    /**
     * @var array
     */
    protected array $loadedLocaleArrays = [];


    /**
     * @var Translator[]
     */
    protected array $translators = [];

    /**
     * @var string
     */
    protected static string $configNamespace = 'core';

    /**
     * @var string
     */
    protected static string $configDir = 'framework';

    /**
     * @var string
     */
    protected static string $configName = 'application';

    /**
     * @var null|Manager
     */
    protected $databaseCapsule = null;

    /**
     * @var null|Request|FormRequest|IlluminateHttpRequest
     */
    protected $request = null;

    /**
     * @var null|LanguageInterface
     */
    protected $language = null;

    /**
     * @var null|SiteInterface
     */
    protected $site = null;

    /**
     * @var object[]
     */
    protected array $instances = [];


    /**
     * @var Application
     */
    protected static Application $_instance;

    /**
     * Application constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        self::initDefaultTimeZone();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @return Application
     */
    public static function getInstance():Application
    {
        if (!isset(static::$_instance)) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    /**
     * @throws Exception
     * @return void
     */
    protected static function initDefaultTimeZone(): void
    {
        $config = Application::getConfig(true, true);

        if($config->has('timezone')) {
            date_default_timezone_set($config->get('timezone'));
        }
        else {
            date_default_timezone_set('UTC');
        }
    }

    /**
     * @param string $locale
     * @param string $group
     * @return bool
     * @throws Exception
     */
    protected static function hasLocaleArrayFile(string $locale, string $group): bool
    {
        $config = Application::getConfig(true,true);

        return file_exists(self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$locale.'/'.$group.'.php');
    }

    /**
     * @param string $locale
     * @param string $group
     * @return Translator
     * @throws Exception
     */
    public function getTranslatorByLocale(string $locale, string $group): Translator
    {
        if(!array_key_exists($locale, $this->translators)) {
            $this->translators[$locale] = new Translator($this->getInstanceOf(ArrayLoader::class), $locale);
        }

        if(!array_key_exists($locale.'_'.$group, $this->loadedLocaleArrays)) {
            $this->loadedLocaleArrays[$locale.'_'.$group] = true;
            if(self::hasLocaleArrayFile($locale, $group)) {
                $config = Application::getConfig(true,true);
                $localeArray = require_once self::systemRootDirPath().'/'.$config->get('localizations_dir').'/'.$locale.'/'.$group.'.php';
                $this->getInstanceOf(ArrayLoader::class)->addMessages($locale, $group, $localeArray);
            }

        }

        return $this->translators[$locale];
    }

    /**
     * @param Manager $capsule
     * @return void
     */
    public function setDatabaseCapsule(Manager $capsule): void
    {
        $this->databaseCapsule = $capsule;
    }

    /**
     * @return null|Manager
     */
    public function getDatabaseCapsule()
    {
        return $this->databaseCapsule;
    }

    /**
     * @return bool
     */
    public function hasDatabaseCapsule():bool
    {
        return !is_null($this->databaseCapsule);
    }

    /**
     * @param FormRequest|IlluminateHttpRequest|Request|null $request
     */
    public function setRequest($request): void
    {
        $this->request = $request;
    }

    /**
     * @return FormRequest|IlluminateHttpRequest|Request|null
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function hasRequest(): bool
    {
        return (null !== $this->request);
    }

    /**
     * @param LanguageInterface $language
     * @return void
     */
    public function setLanguage(LanguageInterface $language): void
    {
        $this->language = $language;
    }

    /**
     * @return LanguageInterface|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function hasLanguage(): bool
    {
        return (null !== $this->language);
    }

    /**
     * @param SiteInterface $site
     */
    public function setSite(SiteInterface $site): void
    {
        $this->site = $site;
    }

    /**
     * @return bool
     */
    public function hasSite(): bool
    {
        return (null !== $this->site);
    }

    /**
     * @return SiteInterface|null
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function env(): string
    {
       return Core::envType();
    }

    /**
     * @param string $key
     * @param array $parameters
     * @param string $group
     * @return string
     * @throws LanguageNullException|Exception
     */
    public function trans(string $key, array $parameters = [],string $group = 'app'): string
    {
        if(!$this->hasLanguage())
            throw new LanguageNullException();

        $translator = $this->getTranslatorByLocale($this->getLanguage()->getLocale(), $group);


        if(array_key_exists('attribute', $parameters)) {
            $attributeKey = $group . '.attributes.' . $parameters['attribute'];
            $attributeTranslation = $translator->get($attributeKey, [], $this->getLanguage()->getLocale());

            if($attributeTranslation !== $attributeKey)
                $parameters['attribute'] = $attributeTranslation;

            $customKey = $group . '.custom.' . $parameters['attribute'] . '.' . $key;
            $customTranslation = $translator->get($customKey, $parameters, $this->getLanguage()->getLocale());

            if($customTranslation !== $customKey)
                return $customTranslation;
        }

        return $translator->get($group . '.' . $key, $parameters, $this->getLanguage()->getLocale());
    }

    /**
     * @param string $key
     * @param array $parameters
     * @param string $group
     * @return string
     * @throws LanguageNullException|Exception
     */
    public static function t(string $key, array $parameters = [],string $group = 'app'): string
    {
        return static::getInstance()->trans($key, $parameters, $group);
    }

    /**
     * @param string $className
     * @return object
     */
    public function getInstanceOf(string $className)
    {
        if(!array_key_exists($className, $this->instances))
            $this->instances[$className] = new $className();

        return $this->instances[$className];
    }

    /**
     * @param string $className
     * @param object $object
     */
    public function setInstanceOf(string $className, $object)
    {
        $this->instances[$className] = $object;
    }

    /**
     * @param string $className
     * @return bool
     */
    public function hasInstanceOf(string $className): bool
    {
        return array_key_exists($className, $this->instances);
    }

    /**
     * @param array $commandsList
     * @throws Exception
     * @return void
     */
    protected function commandsRun(array $commandsList): void
    {
        try {
            foreach ($commandsList as $command) {
                $command = explode('::',$command, 2);
                if(count($command) == 2) {
                    $method = new ReflectionMethod($command[0], $command[1]);
                    $params = $method->getParameters();
                    $argVals = [];

                    foreach ($params as $par) {
                        $class = $par->getType();
                        if(!is_null($class)) {
                            $className = $class->getName();
                            if($className == DatabaseContainerInterface::class || in_array(DatabaseContainerInterface::class, self::getClassParents($className))) {
                                $argVals[] = $this;
                            }
                            elseif ($par->isDefaultValueAvailable()) {
                                $argVals[] = $par->getDefaultValue();
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if($method->isStatic()) {
                        $method->invokeArgs(null, $argVals);
                    }
                    else {
                        $obj = new $command[0]->getInstance();
                        $method->invokeArgs($obj, $argVals);
                    }
                }
                else {
                    $function = new \ReflectionFunction($command);
                    $params = $function->getParameters();
                    $argVals = [];

                    foreach ($params as $par) {
                        $class = $par->getType();
                        if(!is_null($class)) {
                            $className = $class->getName();
                            if($className == DatabaseContainerInterface::class || in_array(DatabaseContainerInterface::class, self::getClassParents($className))) {
                                $argVals[] = $this;
                            }
                            elseif ($par->isDefaultValueAvailable()) {
                                $argVals[] = $par->getDefaultValue();
                            }
                            else {
                                break;
                            }
                        }
                    }

                    $function->invokeArgs($argVals);
                }
            }
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function beforeRun(): void
    {
        $config = self::getConfig(true, true);

        if($config->has('before_run') && !is_null($config->get('before_run'))) {
            $this->commandsRun($config->get('before_run'));
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function afterRun(): void
    {
        $config = self::getConfig(true, true);

        if($config->has('after_run') && !is_null($config->get('after_run'))) {
            $this->commandsRun($config->get('after_run'));
        }

    }
}