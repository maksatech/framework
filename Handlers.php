<?php
namespace Maksatech\Framework;


use Illuminate\Database\Capsule\Manager;
use Maksatech\Containers\DatabaseContainerInterface;
use Illuminate\Config\Repository as Config;
use Illuminate\Support\Facades\Schema;
use Exception;

/**
 * Class Handlers
 * @package Maksatech\Framework
 */
class Handlers
{
    /**
     * @var Handlers
     */
    protected static Handlers $_instance;

    /**
     * Handlers constructor.
     */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance(): Handlers
    {
        if(self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param DatabaseContainerInterface|null $databaseContainer
     * @throws Exception
     */
    public static function initDatabaseConfig(DatabaseContainerInterface $databaseContainer = null): void
    {
        $appConfig = Application::getConfig(true, true);
        if($appConfig->has('database')) {
            $app = new \Illuminate\Foundation\Application();

            $config = new Config();

            $config->set('database.default', 'default');
            $config->set('database.connections', [
                'default' => $appConfig->get('database')
            ]);

            $app->offsetSet('config', $config);

            $serviceProvider = new \Illuminate\Database\DatabaseServiceProvider($app);
            $serviceProvider->register();

            Schema::setFacadeApplication($app);

            $capsule = new Manager($app);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
            if(!is_null($databaseContainer)) {
                $databaseContainer->setDatabaseCapsule($capsule);
            }
        }
    }
}